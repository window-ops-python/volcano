# Volcano

A 3D volcano.

## Installation

To run this project, you will need to have Python and Matplotlib installed on your computer. You can download Python from the official website, and install Matplotlib using pip:

```
pip install matplotlib
```

## Usage

To run the project, simply run the `plot.py` file using Python:

```
python plot.py
```
